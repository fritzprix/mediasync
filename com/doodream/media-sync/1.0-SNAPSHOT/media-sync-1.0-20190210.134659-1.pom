<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.doodream</groupId>
    <artifactId>media-sync</artifactId>
    <version>1.0-SNAPSHOT</version>

    <packaging>jar</packaging>

    <pluginRepositories>
        <pluginRepository>
            <id>synergian-repo</id>
            <url>https://raw.github.com/synergian/wagon-git/releases</url>
        </pluginRepository>
    </pluginRepositories>


    <repositories>
        <repository>
            <id>yarmi-release</id>
            <name>yarmi-release</name>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <url>https://raw.githubusercontent.com/fritzprix/yarmi/releases</url>
        </repository>
        <repository>
            <id>yarmi-dev</id>
            <name>yarmi-dev</name>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <url>https://raw.githubusercontent.com/fritzprix/yarmi/sanpshots</url>
        </repository>
    </repositories>

    <name>media-sync</name>
    <description>self-configured distributed media playback application</description>
    <url>https://github.com/BLANCNOIRVENDY/mediasync</url>

    <licenses>
        <license>
            <name>Apache License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <distributionManagement>
        <repository>
            <id>yarmi-core</id>
            <name>yarmi</name>
            <url>git:releases://git@gitlab.com:fritzprix/mediasync.git</url>
        </repository>
        <snapshotRepository>
            <id>yarmi-core</id>
            <name>yarmi</name>
            <url>git:snapshots://git@gitlab.com:fritzprix/mediasync.git</url>
        </snapshotRepository>
        <site>
            <id>yarmi-core</id>
            <url>git:site://git@gitlab.com:fritzprix/mediasync.git</url>
        </site>
    </distributionManagement>
    <scm>
        <url>https://github.com/BLANCNOIRVENDY/mediasync</url>
    </scm>
    <build>
        <extensions>
            <extension>
                <groupId>ar.com.synergian</groupId>
                <artifactId>wagon-git</artifactId>
                <version>0.2.5</version>
            </extension>
        </extensions>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
            <resource>
                <directory>src/test/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.21.0</version>
                <configuration>
                    <parallel>methods</parallel>
                    <threadCount>10</threadCount>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>2.6</version>
            </plugin>
            <plugin>
                <artifactId>maven-deploy-plugin</artifactId>
                <version>2.8.1</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                        <configuration>
                            <descriptorRefs>
                                <descriptorRef>jar-with-dependencies</descriptorRef>
                            </descriptorRefs>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.6.1</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>com.doodream</groupId>
            <artifactId>yarmi-core</artifactId>
            <scope>provided</scope>
            <version>0.0.6</version>
        </dependency>
    </dependencies>

</project>